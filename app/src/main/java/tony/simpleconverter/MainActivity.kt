package tony.simpleconverter

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.gson.JsonParser
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.HttpException
import tony.simpleconverter.Converters.LocalConverter
import tony.simpleconverter.Converters.RemoteConverter

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    val TAG = javaClass.simpleName
    val mRootLayout: View by lazy { findViewById<View>(R.id.root) }

    val mUnits= arrayListOf<Unit>(
        Kelvin(),
        Celsius(),
        Fahrenheit(),
        Litre(),
        Millilitre(),
        Gallon()
    )

    var mSrcUnit: Unit = mUnits[0]
    set(newValue){
        Log.d(TAG, "src unit is ${newValue.javaClass.simpleName}")
        field = newValue
    }

    var mDesUnit: Unit = mUnits[0]
        set(newValue){
            Log.d(TAG, "des unit is ${newValue.javaClass.simpleName}")
            field = newValue
        }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        //do nothing
    }

    override fun onItemSelected(adapterView: AdapterView<*>, view: View, pos: Int, id: Long) {
        when(adapterView.id){
            R.id.srcUnit -> {
                Log.d(TAG, "src")

                mSrcUnit = mUnits[pos]
                desReading.setText("Click \"CONVERT\" button below")
            }

            R.id.desUnit -> {
                Log.d(TAG, "des")

                mDesUnit = mUnits[pos]

                desReading.setText("Click \"CONVERT\" button below")
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initSpinners()

    }


    private fun initSpinners(){
        srcUnit.onItemSelectedListener = this
        desUnit.onItemSelectedListener = this

        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, resources.getStringArray(R.array.units))
        srcUnit.adapter = arrayAdapter
        desUnit.adapter = arrayAdapter

        srcReading.setText("0.00")
        desReading.setText("")
    }


    fun onConvertClicked(v: View){

        val converter = when(converters.checkedRadioButtonId){
            R.id.radioLocal -> LocalConverter()
            R.id.radioRemote -> RemoteConverter()
            else -> null
        }

        converter?.apply {
            showLoadingUI()

            desReading.setText("")
            val reading = srcReading.text.toString().toDouble()
            Value(mSrcUnit, reading).convertTo(mDesUnit, converter)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {value ->
                        desReading.text = "${value.reading}"
                        hideLoadingUI()
                    },

                    {err ->
                        Log.e(TAG, err.localizedMessage)
                        hideLoadingUI()

                        val errMsg = when (err){

                            is HttpException -> {
                                val jsonString = err.response().errorBody()?.string()

                                val parser = JsonParser();
                                val json = parser.parse(jsonString)

                                json.asJsonObject["msg"].asString

                            }

                            else -> err.localizedMessage
                        }

                        Snackbar.make(mRootLayout, errMsg, Snackbar.LENGTH_SHORT).show()
                    }

                )

        }

    }

    private fun showLoadingUI(){
        convertButton.isEnabled = false
        loading.visibility = View.VISIBLE

    }

    private fun hideLoadingUI(){

        convertButton.isEnabled = true
        loading.visibility = View.INVISIBLE


    }

}
