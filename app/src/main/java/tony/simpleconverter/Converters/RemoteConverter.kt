package tony.simpleconverter.Converters

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path
import tony.simpleconverter.*
import java.lang.IllegalArgumentException

/**
 * RemoteConverter converts values in different units remotely by hitting an endpoint
 * "https://us-central1-delix-io.cloudfunctions.net/api/convertTo/{desUnit}"
 */
class RemoteConverter: IConverter {
    override fun generateObservable(inputValue: Value, outputUnitClassName: String): Observable<Value> {
        val srcUnitSimpleName  = inputValue.unit.javaClass.simpleName;
        val desUnitSimpleName = Class.forName(outputUnitClassName).simpleName
        val convertInfo = ConvertInfo(srcUnitSimpleName, inputValue.reading);

        return APIs.convert(convertInfo, desUnitSimpleName)
            .map {convertInfo ->

                val unit = when(convertInfo.unit){
                    "Kelvin" -> Kelvin()
                    "Celsius" -> Celsius()
                    "Fahrenheit" -> Fahrenheit()

                    "Litre" -> Litre()
                    "Millilitre" -> Millilitre()
                    "Gallon" -> Gallon()

                    else -> null
                }

                unit?.let {
                    Value(unit, convertInfo.reading)
                } ?: kotlin.run {
                    throw IllegalArgumentException("Unkonwn unit ${convertInfo.unit}")
                }



            }
            .subscribeOn(Schedulers.io())
    }
}

const val BASE_URL = "https://us-central1-delix-io.cloudfunctions.net/"
val APIs : IApis by lazy {
    Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build().create(IApis::class.java)
}

data class ConvertInfo(val unit: String="", val reading: Double = 0.0)

interface IApis {

    @POST("api/convertTo/{outputUnit}")
    fun convert(@Body inputValue: ConvertInfo, @Path("outputUnit") destUnit: String): Observable<ConvertInfo>

}