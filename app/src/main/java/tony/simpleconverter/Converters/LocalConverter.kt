package tony.simpleconverter.Converters

import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.schedulers.Schedulers
import tony.simpleconverter.IConverter
import tony.simpleconverter.UnconvertibleException
import tony.simpleconverter.Unit
import tony.simpleconverter.Value

/**
 * LocalConverter converts values in different units locally
 */
class LocalConverter: IConverter {
    override fun generateObservable(inputValue: Value, outputUnitClassName: String): Observable<Value> {

        val unit = Class.forName(outputUnitClassName).newInstance() as Unit

        return Observable.create(ObservableOnSubscribe<Value> { emitter ->

            inputValue.unit.getConversionRule(outputUnitClassName)?.let { rule ->

                val reading = rule.invoke(inputValue.reading)

                emitter.onNext(Value(unit, reading))
                emitter.onComplete()

            } ?: kotlin.run {
                throw UnconvertibleException("${inputValue.unit.javaClass.simpleName} cannot be converted to ${unit::class.java.simpleName}")
            }

        }).subscribeOn(Schedulers.computation())
    }
}