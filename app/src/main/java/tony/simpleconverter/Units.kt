package tony.simpleconverter

private const val ABSOLUTE_ZERO = -273.15

typealias Rule = (src: Double)->Double

abstract class Unit(){
    abstract fun getConversionRule(className:String): Rule?
}

class Kelvin: Unit() {
    override fun getConversionRule(className: String): Rule? {

        return when(className){

            Kelvin::class.java.name -> { src->src}
            Celsius::class.java.name -> { src-> src + ABSOLUTE_ZERO }
            Fahrenheit::class.java.name -> { src-> ((src + ABSOLUTE_ZERO)*1.8)+32}

            else -> null
        }
    }

}

class Celsius: Unit() {
    override fun getConversionRule(className: String): Rule? {

        return when(className){

            Kelvin::class.java.name -> { src-> src - ABSOLUTE_ZERO }
            Celsius::class.java.name -> { src->src}
            Fahrenheit::class.java.name -> { src->(src*1.8)+32}

            else -> null
        }

    }
}

class Fahrenheit: Unit() {
    override fun getConversionRule(className: String): Rule? {

        return when(className){

            Kelvin::class.java.name -> { src->(src-32)/1.8 - ABSOLUTE_ZERO }
            Celsius::class.java.name -> { src->(src-32)/1.8}
            Fahrenheit::class.java.name -> { src->src}

            else -> null
        }

    }

}


class Litre: Unit() {
    override fun getConversionRule(className: String): Rule? {

        return when(className){

            Litre::class.java.name -> { src->src}
            Millilitre::class.java.name -> { src-> src * 1000}
            Gallon::class.java.name -> { src-> src * 0.264172}

            else -> null
        }

    }


}
class Millilitre: Unit() {
    override fun getConversionRule(className: String): Rule? {

        return when(className){
            Litre::class.java.name -> { src->src/1000}
            Millilitre::class.java.name -> { src-> src}
            Gallon::class.java.name -> { src-> src * 0.000264172}

            else -> null
        }

    }


}

class Gallon: Unit() {
    override fun getConversionRule(className: String): Rule? {

        return when(className){

            Litre::class.java.name -> { src->src * 3.78541}
            Millilitre::class.java.name -> { src-> src * 3785.41}
            Gallon::class.java.name -> { src-> src}

            else -> null
        }

    }

}