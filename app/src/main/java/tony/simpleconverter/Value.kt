package tony.simpleconverter

import io.reactivex.Observable

/**
 * A value is the data structure that includes Unit and Reading
 * e.g.  1 kg,  5 km, in which, 1 and 5 are the readings. kg & km are the units
 */
data class Value(val unit: Unit, var reading: Double = 0.0) : IConvertible {

    override fun convertTo(outputUnit: Unit, converter: IConverter): Observable<Value> {
        return converter.generateObservable(this, outputUnit::class.java.name)
    }

    override fun toString(): String {
        return "${unit.javaClass.simpleName} : $reading"
    }
}