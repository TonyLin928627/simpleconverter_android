package tony.simpleconverter

import io.reactivex.Observable
import java.lang.IllegalArgumentException

interface IConverter{
    fun generateObservable(inputValue: Value, outputUnitClassName: String): Observable<Value>
}

interface IConvertible{
    fun convertTo(outputUnit: Unit, converter: IConverter): Observable<Value>
}


class UnconvertibleException(msg: String?) : IllegalArgumentException(msg)